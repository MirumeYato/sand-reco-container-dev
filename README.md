# Sand-reco-container-dev

This project conteins only container registry. You can find it in left bar "Deploy" -> "Container Registry"

Now you can find full version v1.0 with folder "data" with test data for StrangeResonanse analytical project. Version v1.1-light don't have it so you should load your own data.

## Adding data
use commads on your PC:
```
#container_id can be found from this cmd:
docker ps
docker cp path/from_your_computer/to/sand_fhc_nom_stt_cc_00001.10.edep.root" <container_id>:/root/data
```

## Getting started
If you want to run smth you need firstly source path to ROOT,edep-sim,Geant4 and sand-reco
```
source /root/StrangeResonance/setup.sh
```
Than you can load your data or use test data file /root/data/sand_fhc_nom_stt_cc_00001.10.edep.root in v01.0 version. Process described in https://github.com/DUNE/sandreco/tree/develop.
```
cd /root/data
Digitize sand_fhc_nom_stt_cc_00001.10.edep.root sand_test_digit.root
Reconstruct sand_fhc_nom_stt_cc_00001.10.edep.root sand_test_digit.root sand_test_reco.root
Analyze sand_fhc_nom_stt_cc_00001.10.edep.root sand_test_reco.root
FastCheck sand_test_reco.root test.pdf
```
Now you can use your data in StrangeResonance project:
```
cd /root/StrangeResonance
# Here you can change analysis in /root/StrangeResonance/src/StrangeResonance.cxx to code that you need
cmake -DCMAKE_INSTALL_PREFIX=./.. ./
make
./resonanceAnalyse -f /root/data/sand_test_reco.root
```
